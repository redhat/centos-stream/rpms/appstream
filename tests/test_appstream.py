import unittest
import sys
import subprocess

class TestAppstream(unittest.TestCase):

    def test_dummy(self):
        self.assertEqual(True, True)

    @unittest.expectedFailure
    def test_expected_failure(self):
        self.assertEqual(False, True)

    def test_appstreamcli_search(self):
        cp  = subprocess.run(
            ['env LC_ALL=en_US.UTF-8 appstreamcli search ibus-typing-booster'],
            encoding='UTF-8',
            text=True,
            shell=True,
            capture_output=True)
        print('----------------------------------------')
        print(cp.stdout)
        print('----------------------------------------')
        output_lines = cp.stdout.split('\n')
        result_dict = {}
        identifier = ''
        for line in output_lines:
            if line == '---' or ':' not in line:
                continue
            key, value = map(str.strip, line.split(':', maxsplit=1))
            if key == 'Identifier':
                identifier = value
                result_dict[identifier] = {}
            result_dict[identifier][key] = value
        print('----------------------------------------')
        print(result_dict)
        print('----------------------------------------')
        typing_booster_identifier = (
            'org.freedesktop.ibus.engine.typing_booster [inputmethod]')
        print(f'Assert that {typing_booster_identifier} in {result_dict.keys()}')
        self.assertTrue(
            typing_booster_identifier
            in result_dict.keys())
        expected_dict = {
            typing_booster_identifier: {
                'Identifier': typing_booster_identifier,
                'Name': 'Typing Booster',
                'Summary': 'Predictive input method',
                # The 'Package' section seems to be not always there,
                # better do not test for this:
                # 'Package': 'ibus-typing-booster',
                'Homepage': 'https://mike-fabian.github.io/ibus-typing-booster/',
                'Icon': 'org.freedesktop.ibus.engine.typing_booster.png',
            }
        }
        for result_key, result_value in result_dict.items():
            if result_key == typing_booster_identifier:
                for expected_key, expected_value in (
                        expected_dict[typing_booster_identifier].items()):
                    print(f'Assert that {expected_key} is {expected_value}:')
                    self.assertEqual(
                        result_dict[result_key][expected_key],
                        expected_value)

if __name__ == "__main__":
    unittest.main()
